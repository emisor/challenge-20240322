<?php

use Livewire\Volt\Component;
use App\Models\Executed;

new class extends Component {
    public function with(): array
    {
        return [
            'executed' => Executed::orderBy('id', 'desc')->first(),
        ];
    }
}; ?>

<div>
    <h4>Órdenes de Compra</h4>
    <hr/>
    <livewire:orders.list />
    @if($executed)
        <p>
            Pedidos: {{ $executed->total_orders }} - Total: {{ $executed->total_cost }} - {{ $executed->created_at->format('d/m/Y H:i:s') }}
        </p>
    @endif
</div>
