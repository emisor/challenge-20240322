<?php

namespace App\Jobs;

use App\Models\Executed;
use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class TallyOrders implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $tallies = Product::query()->join('orders_lines', 'products.id', '=', 'orders_lines.product_id')->select([
            DB::raw("COUNT(DISTINCT(orders_lines.order_id)) AS total_orders"),
            DB::raw("SUM(orders_lines.qty * products.cost) AS total_cost"),
        ])->first();

        $result = Http::post(route('api.executed.create', []), $tallies);
    }
}
