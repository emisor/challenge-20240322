<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    public function orderLines()
    {
        return $this->belongsToMany(Product::class, 'orders_lines')->withPivot('qty')->withTimestamps();
    }

    public function getTotalAttribute() {
        return $this->orderLines->map(function ($orderLine) {
            return $orderLine->pivot->qty * $orderLine->cost;
        })->sum();
    }
}
