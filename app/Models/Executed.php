<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Executed extends Model
{
    protected $table = 'executed';
    protected $fillable = [
        'total_orders',
        'total_cost',
    ];
}
