<?php

namespace App\Console\Commands;

use App\Jobs\TallyOrders;
use Illuminate\Console\Command;

class QueueOrderTallying extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'execute:total';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calcula el coste total de todos los pedidos de la DB';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $job = (new TallyOrders)->onQueue('sync');
        $job->dispatch();
    }
}
