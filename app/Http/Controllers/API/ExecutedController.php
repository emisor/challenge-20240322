<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Executed;
use Illuminate\Http\Request;

class ExecutedController extends Controller
{
    public function create(Request $request) {
        $request->validate([
            'total_orders' => ['required', 'integer'],
            'total_cost' => ['required', 'numeric'],
        ]);
        Executed::create($request->only('total_orders', 'total_cost'));
    }
}

