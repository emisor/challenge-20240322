<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Order;
use App\Models\Product;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $products = Product::factory(10)->create();

        Order::factory(20)->create()->each(function ($order) use ($products) {
            $line_products = $products->random(rand(1, 5));
            foreach ($line_products as $product) {
                $order->orderLines()->attach($product->id, ['qty' => rand(1, 10)]);
            }
        });
    }
}
